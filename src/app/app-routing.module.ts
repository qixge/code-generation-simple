import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

const routes: Routes = [
  { path: '', pathMatch: 'full', redirectTo: '/readme' },
  {
    path:'readme',
    loadChildren:()=>import('./pages/readme/readme.module').then(m=>m.ReadmeModule),
  },
  {
    path: 'delon-code',
    loadChildren: () =>
      import('./pages/delon-code/delon-code.module').then((m) => m.DelonCodeModule),
  },{
    path: 'comm-code',
    loadChildren: () =>
      import('./pages/comm-code/comm-code.module').then((m) => m.CommCodeModule),
  },
];

@NgModule({
  imports: [RouterModule.forRoot(routes, { useHash: true })],
  exports: [RouterModule],
})
export class AppRoutingModule { }

import { Component, Injector } from '@angular/core';
import { Router } from '@angular/router';

export interface CGTMenuItem {
  title?: string;  // 显示标题
  icon?: string;  // 图标
  routeUrl?: string;   // 路由地址
  children?: CGTMenuItem[]    // 下级菜单
}

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.less']
})
export class AppComponent {

  isCollapsed = false;

  menuItems: CGTMenuItem[] = [
    {
      title: 'Delon Code',
      icon: 'user',
      children: [
        {
          title: 'SFSchema',
          routeUrl: '/delon-code/sf',
        },
        {
          title: 'STColumn',
          routeUrl: '/delon-code/st',
        }
      ]
    },
    {
      title: 'Comm Code',
      icon: 'user',
      children: [
        {
          title: 'Interface',
          routeUrl: '/comm-code/interface',
        },
        {
          title: 'JsonSchema',
          routeUrl: '/comm-code/json-to-schema',
        }
      ]
    }
  ];

  constructor(private injector: Injector, private router: Router) { }


  onSelect(url: string) {
    // console.log(url);
    this.router.navigateByUrl(url);
  }
}

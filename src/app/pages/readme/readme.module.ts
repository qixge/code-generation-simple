import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {ReadmeComponent} from './readme.component';
import {RouterModule} from "@angular/router";


@NgModule({
    declarations: [
        ReadmeComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([{path: '', component: ReadmeComponent}])
    ]
})
export class ReadmeModule {
}

import { Component, Injector, OnInit } from '@angular/core';
import { BaseOfDelonCodeComponent } from '../abstract-delon-code.component';
import { STJsonObj } from '../services/interface';

@Component({
  selector: 'app-delon-st-code',
  templateUrl: './st-code.component.html',
  styleUrls: ['./st-code.component.scss']
})
export class StCodeComponent extends BaseOfDelonCodeComponent implements OnInit {

  /**
   * swagger生成的form条件对象
   */
  stCode = '';


  constructor(public injector: Injector) { super(injector); }

  ngOnInit(): void {
  }




  // 生成代码片段
  codeGeneration(): void {
    // 加入
    this.dcSrv.checkAndMakeSTColumn(this.stCode)
      .then((res: STJsonObj[]) => {
        this.showCode(res, 'STColumn[]');
      }).catch((e: Error) => {
        this.modalSrv.showErrorModal(e.message);
      });
  }

}

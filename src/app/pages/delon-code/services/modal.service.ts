import { Injectable, Injector } from '@angular/core';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ShowCodeJsonComponent } from '../json/json.component';
import { SFJsonObj, STJsonObj } from './interface';

/**
 * 通用模态框显示
 */
@Injectable({
  providedIn: 'root'
})
export class CGModalService {


  constructor(
    private service: NzModalService,
    private injector: Injector
  ) { }

  showErrorModal(msg: string) {
    this.service.error({
      nzTitle: `操作失败！`,
      nzZIndex: 1200,
      nzContent: msg,
    });
  }

  showCodeWindow(arg: { title: string; type: string; obj: SFJsonObj | STJsonObj[]; }) {
    this.service.create({
      nzTitle:arg.title,
      nzContent:ShowCodeJsonComponent,
      nzData:{
        jsonObj: arg.obj
      },
      nzWidth:700,
      nzFooter:null
    });
  }
}

import { Injectable, Injector } from "@angular/core";
import { SFJsonObj, STJsonObj } from "./interface";

@Injectable({ providedIn: 'platform' })
export class DelonCodeService {

  constructor(private injector: Injector) { }

  checkAndMakeSTColumn(str: string): Promise<STJsonObj[]> {
    return new Promise((resolve, reject) => {
      const obj = JSON.parse(str);
      if (typeof obj === "object" && Object.keys(obj).length > 0) {
        // 异步操作成功 resolve
        const st = this.makeSTColumn(obj);
        resolve(st);
      } else {
        // 异步操作失败 reject
        reject(new Error("Json对象解析错误！"));
      }
    });
  }

  // 生成SFSchema代码
  private makeSTColumn = (obj: STJsonObj): STJsonObj[] => {
    const newObj: any[] = [];
    Object.keys(obj).forEach((key: string | number) => {
      newObj.push({
        title: obj[key]['description'] ?? key,
        index: key,
        className: 'text-center',
      });
    });

    return newObj;
  };

  // 验证字符串是否是json对象字符串
  checkAndMakeSFSchema(str: string): Promise<SFJsonObj> {
    return new Promise((resolve, reject) => {
      const obj = JSON.parse(str);
      if (typeof obj === "object" && Object.keys(obj).length > 0) {
        // 异步操作成功 resolve
        const sf = this.makeSFSchema(obj);
        resolve(sf);
      } else {
        // 异步操作失败 reject
        reject(new Error("Json对象解析错误！"));
      }
    });
  }

  // 生成SFSchema代码
  private makeSFSchema = (obj: SFJsonObj): SFJsonObj => {
    const newObj = {
      properties: {} as any,
    } as any;
    Object.keys(obj).forEach((key: string | number) => {
      newObj.properties[key] = {
        title: obj[key]['description'] ?? key,
        type: obj[key]['type'] ?? this.makeSFSchemaType(obj[key]),
        ui: {
          grid: {
            span: 12
          }
        }
      };
    });
    newObj.required = Object.keys(obj);
    newObj.ui = {
      spanLabel: 6,
      spanControl: 18,
      grid: {
        span: 24
      }
    };
    return newObj;
  };

  // 生成SFSchema的type
  private makeSFSchemaType = (key: string | number): string => {
    switch (typeof key) {
      case "string":
        return "string";
      case "number":
        return "number";
    }
  };
}

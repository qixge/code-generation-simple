import {Component, Inject, Injector, Input, OnInit} from '@angular/core';
import { NzMessageService } from 'ng-zorro-antd/message';
import {NZ_MODAL_DATA} from "ng-zorro-antd/modal";
/**
 * 用于展示json代码
 */
@Component({
  selector: 'app-show-code-json',
  templateUrl: './json.component.html',
  styleUrls: ['./json.component.scss']
})
export class ShowCodeJsonComponent implements OnInit {


  @Input()
  jsonObj: any;


  // 需要展示的json对象
  content!: string;

  constructor(public injector: Injector, private message: NzMessageService, @Inject(NZ_MODAL_DATA) private params: any) {
    this.jsonObj = this.params.jsonObj;
  }

  ngOnInit(): void {
    this.content = this.jsonShowFn(this.jsonObj);
  }

  jsonShowFn(json: any): string {

    if (typeof json !== 'string') {
      json = JSON.stringify(json, undefined, 2);
    }

    json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
    return json.replace(
      /("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g,
      (match: string) => {
        let cls = 'color: darkorange;';
        if (/^"/.test(match)) {
          if (/:$/.test(match)) {
            cls = 'color: red;';
          } else {
            cls = 'color: green;';
          }
        } else if (/true|false/.test(match)) {
          cls = 'color: blue;';
        } else if (/null/.test(match)) {
          cls = 'color: magenta;';
        }
        return '<span style="' + cls + '">' + match + '</span>';
      });
  }

  handleCopyToClipboard(copyWord: HTMLElement) {
    const range = document.createRange();
    range.selectNode(copyWord);
    window?.getSelection()?.removeAllRanges();
    window?.getSelection()?.addRange(range);
    document.execCommand('copy');
    this.message.success('复制成功');
    window?.getSelection()?.removeAllRanges();
  }

}

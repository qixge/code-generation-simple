import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { Routes, RouterModule } from '@angular/router';
import { StCodeComponent } from './st-code/st-code.component';
import { SfCodeComponent } from './sf-code/sf-code.component';


const routes: Routes = [
    {
        path: 'st',
        component: StCodeComponent,
        data: {
            breadcrumb: 'STColumn Code'
        }
    },
    {
        path: 'sf',
        component: SfCodeComponent,
        data: {
            breadcrumb: 'SFSchema Code'
        }
    }
];

@NgModule({
    declarations: [],
    imports: [CommonModule, RouterModule.forChild(routes)],
    exports: [RouterModule]
})
export class DelonCodeRoutingModule { }

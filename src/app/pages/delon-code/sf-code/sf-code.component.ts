import { Component, Injector, OnInit } from '@angular/core';
import { BaseOfDelonCodeComponent } from '../abstract-delon-code.component';
import { SFJsonObj } from '../services/interface';

@Component({
  selector: 'app-delon-sf-code',
  templateUrl: './sf-code.component.html',
  styleUrls: ['./sf-code.component.scss']
})
export class SfCodeComponent extends BaseOfDelonCodeComponent implements OnInit {

  /**
   * swagger生成的form条件对象
   */
  sfCode = '';


  constructor(public injector: Injector) { super(injector); }

  ngOnInit(): void {
  }




  // 生成代码片段
  codeGeneration(): void {
    // 加入
    this.dcSrv.checkAndMakeSFSchema(this.sfCode)
      .then((res: SFJsonObj) => {
        this.showCode(res, 'SFSchema');
      }).catch((e: Error) => {
        this.modalSrv.showErrorModal(e.message);
      });
  }
}

import { NgModule } from '@angular/core';
import { StCodeComponent } from './st-code/st-code.component';
import { SfCodeComponent } from './sf-code/sf-code.component';
import { DelonCodeRoutingModule } from './delon-code-routing.module';
import { DelonCodeService } from './services/delon-code.service';
import { NzCardModule } from 'ng-zorro-antd/card';
import { NzInputModule } from 'ng-zorro-antd/input';
import { FormsModule } from '@angular/forms';
import { CGModalService } from './services';
import { NzModalService } from 'ng-zorro-antd/modal';
import { ShowCodeJsonComponent } from './json/json.component';


import { NzButtonModule } from 'ng-zorro-antd/button';
import { NzMessageModule, NzMessageService } from 'ng-zorro-antd/message';
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {PipeModule} from "../../comm/pipe/pipe.module";
@NgModule({
  declarations: [StCodeComponent, SfCodeComponent, ShowCodeJsonComponent],
  providers: [DelonCodeService, CGModalService, NzModalService, NzMessageService],
  imports: [
    DelonCodeRoutingModule,
    NzCardModule,
    NzInputModule,
    FormsModule,
    NzButtonModule,
    NzMessageModule,
    NzGridModule,
    NzTypographyModule,
    PipeModule
  ]
})
export class DelonCodeModule { }

import { Directive, OnInit, OnDestroy, Injector } from '@angular/core';
import { CGModalService } from './services';
import { DelonCodeService } from './services/delon-code.service';

import { SFJsonObj, STJsonObj } from './services/interface';

/**
 * ShowCode的基础组件
 */
@Directive()
export abstract class BaseOfDelonCodeComponent implements OnInit, OnDestroy {

  dcSrv: DelonCodeService;
  modalSrv: CGModalService;
  constructor(public injector: Injector) {
    this.dcSrv = this.injector.get(DelonCodeService);
    this.modalSrv = this.injector.get(CGModalService);
  }

  ngOnInit(): void { }

  ngOnDestroy(): void { }

  showCode(jsonCode: SFJsonObj | STJsonObj[], title: string): void {
    this.modalSrv.showCodeWindow({ title: title, type: 'json', obj: jsonCode });
  }
}

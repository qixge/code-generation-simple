import {Component} from '@angular/core';
import {NzModalService} from "ng-zorro-antd/modal";
import {ShowJsCodeComponent} from "../js-code/js-code.component";
import * as TJS from "typescript-json-schema";

@Component({
    selector: 'app-json-to-schema',
    templateUrl: './json-to-schema.component.html',
    styleUrls: ['./json-to-schema.component.less']
})
export class JsonToSchemaComponent {
    jsonCode: string = '';

    constructor(private service: NzModalService,) {
    }

    ngOnInit(): void {
    }

    codeGeneration() {
        this.checkAndMakeJsonSchema(this.jsonCode).then(res => {
            this.service.create({
                nzTitle: 'JsonSchema',
                nzContent: ShowJsCodeComponent,
                nzData: {
                    jsObj: res
                },
                nzWidth: 700,
                nzFooter: null
            });
        });
    }

    checkAndMakeJsonSchema(str: string): Promise<string[]> {
        return new Promise((resolve, reject) => {
            const obj = JSON.parse(str);
            if (typeof obj === "object" && Object.keys(obj).length > 0) {
                // const schema = new SchemaGenerator().generate(obj);

                resolve({});
                // TJS.getProgramFromFiles()
            } else {
                // 异步操作失败 reject
                reject(new Error("Json对象解析错误！"));
            }
        });
    }
}

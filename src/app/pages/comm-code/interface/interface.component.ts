import {Component, OnInit} from '@angular/core';
import {NzModalService} from "ng-zorro-antd/modal";
import {ShowJsCodeComponent} from "../js-code/js-code.component";

@Component({
    selector: 'app-interface',
    templateUrl: './interface.component.html',
    styleUrls: ['./interface.component.less']
})
export class InterfaceComponent implements OnInit {

    interfaceCode: string = '';

    constructor(private service: NzModalService,) {
    }

    ngOnInit(): void {
    }

    codeGeneration() {
        this.checkAndMakeInterface(this.interfaceCode).then(res => {
            this.service.create({
                nzTitle: 'Interface',
                nzContent: ShowJsCodeComponent,
                nzData: {
                    jsObj: res
                },
                nzWidth: 700,
                nzFooter: null
            });
        });
    }

    checkAndMakeInterface(str: string): Promise<string[]> {
        return new Promise((resolve, reject) => {
            const obj = JSON.parse(str);
            if (typeof obj === "object" && Object.keys(obj).length > 0) {
                let interfaceArrStr = [] as string[];
                interfaceArrStr.push("export interface XXXXX {");
                // 异步操作成功 resolve
                Object.keys(obj).forEach((key: string | number) => {
                    if(obj[key]['type'] === 'integer') {
                        obj[key]['type'] = 'number';
                    }
                    const str = '   ' + key + '?:' + obj[key]['type'] + ';' + '// ' + obj[key]['description'] ?? key;
                    interfaceArrStr.push(str);
                });
                interfaceArrStr.push("}");
                resolve(interfaceArrStr);
            } else {
                // 异步操作失败 reject
                reject(new Error("Json对象解析错误！"));
            }
        });
    }


}

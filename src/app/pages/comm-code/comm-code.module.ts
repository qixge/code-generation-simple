import {NgModule} from '@angular/core';
import {CommonModule} from '@angular/common';
import {InterfaceComponent} from './interface/interface.component';
import {RouterModule} from "@angular/router";
import {NzGridModule} from "ng-zorro-antd/grid";
import {NzInputModule} from "ng-zorro-antd/input";
import {NzCardModule} from "ng-zorro-antd/card";
import {FormsModule} from "@angular/forms";
import {NzButtonModule} from "ng-zorro-antd/button";
import {NzTypographyModule} from "ng-zorro-antd/typography";
import {ShowJsCodeComponent} from './js-code/js-code.component';
import {PipeModule} from "../../comm/pipe/pipe.module";
import {NzModalService} from "ng-zorro-antd/modal";
import {NzMessageService} from "ng-zorro-antd/message";
import { JsonToSchemaComponent } from './json-to-schema/json-to-schema.component';


@NgModule({
    declarations: [
        InterfaceComponent,
        ShowJsCodeComponent,
        JsonToSchemaComponent
    ],
    imports: [
        CommonModule,
        RouterModule.forChild([
            {path: 'interface', component: InterfaceComponent},
            {path: 'json-to-schema', component: JsonToSchemaComponent},
        ]),
        NzGridModule,
        NzInputModule,
        NzCardModule,
        FormsModule,
        NzButtonModule,
        NzTypographyModule,
        PipeModule
    ],
    providers: [NzModalService,NzMessageService]
})
export class CommCodeModule {
}

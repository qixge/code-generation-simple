import {Component, Injector, Input, OnInit} from '@angular/core';
import {NzMessageService} from "ng-zorro-antd/message";

@Component({
    selector: 'app-js-code',
    templateUrl: './js-code.component.html',
    styleUrls: ['./js-code.component.less']
})
export class ShowJsCodeComponent implements OnInit {

    @Input()
    jsObj: any[] = [];


    // 需要展示的json对象
    content!: string;

    constructor(public injector: Injector, private message: NzMessageService) {
    }

    ngOnInit(): void {
        this.content = this.jsShowFn(this.jsObj);
    }

    jsShowFn(jsCode: any[]): string {
        return jsCode.join("\n");

    }

    handleCopyToClipboard(copyWord: HTMLElement) {
        const range = document.createRange();
        range.selectNode(copyWord);
        window?.getSelection()?.removeAllRanges();
        window?.getSelection()?.addRange(range);
        document.execCommand('copy');
        this.message.success('复制成功');
        window?.getSelection()?.removeAllRanges();
    }

}
